# OCCAM Web Interface

This project serves as the front-end website for the OCCAM system.

## Project Layout

```
:::text

lib               - application code
\- application.rb - main app and helpers

helpers           - assorted functions to aid views

models            - data models and representation
\- account.rb     - user accounts and authorization

controllers       - url routing
\- accounts.rb    - routes for accounts
\- sessions.rb    - logging on/off routes

views             - html markup
\- accounts       - pages for accounts
\- sessions       - pages for logging on

public            - external assets
|- css            - stylesheets
\- js             - javascript files

spec              - tests
\- controller     - tests for routes and renders
```

## Development

Clone this repo:

```
:::text
git clone https://bitbucket.org/occam/occam-web
```

Inside the project directory, install dependencies:

```
:::text
bundle install
```

Run a development server (by default, this project uses 'thin'):

```
:::text
rackup
```

This will spawn a local webserver listening on port 9292 at localhost:

```
:::text
http://localhost:9292/
```

## Testing

To run the back-end tests, inside the project directory, invoke:

```
:::text
rake test
```

For convenience, several shortcuts are available to speed up testing turn around. To run a particular model/controller test, or even a particular file (in this case, model/controller Foo):

```
:::text
rake test:model[foo]
rake test:controller[foo]
rake test:file[foo.rb]
```

## Open Source

This project makes use of several open source technologies:

* [Ruby](https://www.ruby-lang.org/en/) - an expressive scripting language.
* [Sinatra](http://www.sinatrarb.com/) - a minimalistic web framework for Ruby.
* [thin](http://code.macournoyer.com/thin/) - a minimal webserver we use for development.
* [haml](http://haml.info/) - markup preprocessor for html.
