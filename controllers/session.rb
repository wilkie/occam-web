class Occam < Sinatra::Base
  # Retrieve login form
  get '/login' do
    render :haml, :"sessions/login"
  end

  # Sign on
  post '/login' do
    account = Account.first(:conditions => ["LOWER(username) like ?", params["username"].downcase])
    if account && account.authenticated?(params["password"])
      session[:account_id] = account.id

      redirect '/'
    else
      status 404
    end
  end

  # Sign out
  get '/logout' do
    session[:account_id] = nil

    redirect '/'
  end
end
