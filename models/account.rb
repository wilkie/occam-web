class Account
  require 'bcrypt'

  include DataMapper::Resource

  # Fields

  # Unique identifier
  property :id,              Serial, :key => true,
                                     :unique_index => true

  # Username
  property :username,        String, :length   => 4..30,
                                     :unique   => true,
                                     :required => true

  # Do not allow a duplicate username
  validates_with_method :username, :method => :username_check

  # Hashed password
  property :hashed_password, String, :length   => 60..60,
                                     :required => true

  # Maintains timestamps for creation and modification
  timestamps :created_at,
             :updated_at

  private

  def username_check
    Account.first(:conditions => ["LOWER(username) LIKE ?", self.username.downcase]).nil?
  end

  public

  # Create a hash of the password.
  def self.hash_password(password)
    BCrypt::Password.create(password, :cost => Occam::BCRYPT_ROUNDS)
  end

  # Determine if the given password matches the account.
  def authenticated?(password)
    BCrypt::Password.new(hashed_password) == password
  end

  def initialize(options = {})
    options.delete :hashed_password

    # Hash the password before entering it in our database
    if options[:password]
      options[:hashed_password] = Account.hash_password(options[:password])
      options.delete :password
    end

    super options
  end
end
