require_relative 'helper'

describe Occam do
  describe "Accounts Controller" do
    describe "GET /accounts" do
      it "should pass an array of accounts to the view" do
        Account.create(:username => "wilkie",
                       :password => "foo")
        Account.create(:username => "wilkie2",
                       :password => "foo")
        Account.create(:username => "wilkie3",
                       :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:accounts => DataMapper::Collection)
        )

        get '/accounts'
      end

      it "should query and pass the accounts to the view" do
        Account.create(:username => "wilkie",
                       :password => "foo")

        account = Account.create(:username => "wilkie3",
                                 :password => "foo")

        Account.create(:username => "wilkie2",
                       :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:accounts, account)
        )

        get '/accounts'
      end

      it "should return 200" do
        get '/accounts'

        last_response.status.must_equal 200
      end

      it "should render accounts/index.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/index",
          anything
        )

        get '/accounts'
      end
    end

    describe "GET /accounts/new" do
      it "should render accounts/new.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/new",
          anything
        )

        get '/accounts/new'
      end

      it "should return 200" do
        get '/accounts/new'

        last_response.status.must_equal 200
      end
    end

    describe "GET /accounts/:id" do
      it "should render accounts/show.haml" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/show",
          anything
        )

        get "/accounts/#{account.id}"
      end

      it "should query and pass the given account" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:account, account)
        )

        get "/accounts/#{account.id}"
      end

      it "should return 200 when account is found" do
        account = Account.create(:username => "wilkie",
                                 :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:account, account)
        )

        get "/accounts/#{account.id}"

        last_response.status.must_equal 200
      end

      it "should return 404 when account is not found" do
        get "/accounts/abcd1234"

        last_response.status.must_equal 404
      end
    end

    describe "POST /accounts" do
      it "should create an account with the given username" do
        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        Account.first("username", "wilkie").username.must_equal "wilkie"
      end

      it "should return 302 when account is created" do
        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 302
      end

      it "should redirect to account page when account is created" do
        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        account = Account.first("username", "wilkie")

        last_response.location.must_equal(
          "http://example.org/accounts/#{account.id}")
      end
    end
  end
end
