class Occam
  # Get a list of all accounts
  get '/accounts' do
    accounts = Account.all
    render :haml, :"accounts/index", :locals => {
      :accounts => accounts
    }
  end

  # Create a new account
  post '/accounts' do
    account = Account.create :username => params["username"],
                             :password => params["password"]

    # TODO: check validations
    # if account.errors.any?
    # else
      redirect "/accounts/#{account.id}"
    # end
  end

  # Form to create a new account
  get '/accounts/new' do
    render :haml, :"accounts/new"
  end

  # Retrieve a specific account page
  get '/accounts/:id' do
    account = Account.first(:id => params[:id].to_i)

    if account.nil?
      status 404
    else
      render :haml, :"accounts/show", :locals => {
        :account => account
      }
    end
  end
end
