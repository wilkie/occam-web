class Occam
  get '/' do
    haml :index
  end

  get '/styleguide' do
    haml :styleguide
  end
end
