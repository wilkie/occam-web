source 'https://rubygems.org'
ruby '2.0.0'

# Web Framework
gem 'sinatra', '~> 1.4.2'
gem 'sinatra-contrib'

# Database Abstraction
gem 'data_mapper'

# Markup Rendering Engine
gem 'haml'

# Password hashing
gem 'bcrypt-ruby'

# Development only tools
group :development do
  gem 'rake'         # Runs Rakefiles
  gem 'sqlite3'      # Development Database
  gem 'dm-sqlite-adapter'
  gem 'debugger'     # Breakpoint based debugger
  gem 'ruby-prof'    # Performance monitoring
end

# Testing environment libraries
group :test do
  gem 'capybara', '~> 1.1.2',  :require => 'capybara/dsl'
  gem 'fabrication', '~> 1.2.0'
  gem 'database_cleaner', '~> 0.6.7'
  gem 'rack-test', '~> 0.6.1', :require => 'rack/test'
  gem 'minitest', '~> 4.7.0', :require => 'minitest/autorun'
  gem "ansi"              # minitest colors
  gem "turn"              # minitest output
  gem "mocha"             # stubs
end

# Production level libraries
group :production do
  gem 'pg'
  gem 'dm-postgres-adapter'
end

gem 'thin'
