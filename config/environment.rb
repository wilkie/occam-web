require 'sinatra'
require 'dm-sqlite-adapter'

if ENV['RACK_ENV'] == :production
  require 'pg'
else
  require 'sqlite3'
end

require_relative './application'

class Occam < Sinatra::Base
  configure :test do
    DataMapper.setup(:default, "sqlite::memory:")
  end

  configure :development do
    DataMapper.setup(:default, "sqlite://#{File.expand_path(File.dirname(__FILE__))}/../dev.db")
  end

  configure :production do
    DataMapper.setup(:default, ENV['DATABASE_URL'] || 'postgres:///localhost/occam')
  end
end
