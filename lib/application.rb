require 'bundler'
Bundler::require

require_relative '../config/environment'

# Application root
class Occam < Sinatra::Base
  # Use root directory as root
  set :app_file => '.'

  # Use HTML5
  set :haml, :format => :html5

  # Use sessions
  # TODO: Secret
  use Rack::Session::Cookie, :key => 'rack.session',
                             :path => '/',
                             :secret => 'foobar'

  # Helpers
  helpers Sinatra::ContentFor

  def self.load_tasks
    Dir[File.join(File.dirname(__FILE__), "tasks", '*.rb')].each do |file|
      require file
    end
  end

  helpers do
    def partial(page, options={})
      if page.to_s.include? "/"
        page = page.to_s.sub /[\/]([^\/]+)$/, "/_\\1"
      else
        page = "_#{page}"
      end
      haml page.to_sym, options.merge!(:layout => false)
    end
  end
end

# Load application source
%w(config helpers controllers models).each do |dir|
    Dir[File.join(File.dirname(__FILE__), "../#{dir}", '*.rb')].each {|file| require file }
end

# Finalize
if ENV['RACK_ENV'] == :test
  DataMapper.finalize.auto_migrate!
else
  DataMapper.finalize.auto_upgrade!
end
